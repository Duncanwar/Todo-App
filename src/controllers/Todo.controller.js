import TodoService from '../services/Todo.services';
import Response from '../utils/responses';

export default class TodoController {
  static async addTodo(req, res) {
    try {
      const { title, description } = req.body;
      if (!title || !description) {
        return Response.errorResponse(res, 400, 'empty request');
      }
      let todo = { userId: req.User.id, ...req.body };
      todo = await TodoService.create(todo);
      return Response.successResponse(res, 200, undefined, 'created todo', todo);
    } catch (error) {
      return Response.errorResponse(res, 401, error);
    }
  }

  static async getAll(req, res) {
    try {
      const todo = await TodoService.findAll();
      return Response.successResponse(res, 200, undefined, 'retrieved', todo);
    } catch (error) {
      return Response.errorResponse(res, 401, error);
    }
  }

  static async getOneById(req, res) {
    try {
      const todo = await TodoService.findOneById(req.params.id);
      return Response.successResponse(res, 200, undefined, 'got it', todo);
    } catch (error) {
      return Response.errorResponse(res, 401, error);
    }
  }

  static async update(req, res) {
    try {
      const todo = await TodoService.update(req.body, req.params.id);
      return Response.successResponse(res, 200, undefined, 'update todo', todo);
    } catch (error) {
      return Response.errorResponse(res, 401, error);
    }
  }

  static async deleteOne(req, res) {
    try {
      const todo = await TodoService.delete(req.params.id);
      return Response.successResponse(res, 200, undefined, 'delete todo', todo);
    } catch (error) {
      return Response.errorResponse(res, 401, error);
    }
  }

  static async getTodoListByUser(req, res) {
    try {
      const todo = await TodoService.findAllByUserId(req.User.id);
      return Response.successResponse(res, 200, undefined, 'retrieved', todo);
    } catch (error) {
      return Response.errorResponse(res, 401, error.data);
    }
  }
}
