import UserService from '../services/User.services';
import responses from '../utils/responses';
import statusCode from '../utils/statusCode';
import helper from '../utils/helpers';

const {
  createUser, getUserByIdOrEmail
} = UserService;
const { hashPassword, generateToken } = helper;
const { created, ok } = statusCode;
const { successResponse } = responses;

export default class UserControllers {
  static async signup(req, res, next) {
    try {
      const formData = req.body;
      formData.password = hashPassword(formData.password);
      const user = await createUser(formData);
      return successResponse(res, created, undefined, 'Signup successfully', user);
    } catch (e) {
      return next(new Error(e));
    }
  }

  /**
 * @description
 * @param {object} req
 * @param {object} res
 * @param {object} next
 * @return {object}
 */
  static async login(req, res) {
    const inputFormData = req.body;
    const { dataValues } = await getUserByIdOrEmail(inputFormData.email);
    const {
      password, createdAt, updatedAt, ...user
    } = dataValues;
    const token = generateToken(user);
    return successResponse(res, ok, token, 'logged in', user);
  }
}
