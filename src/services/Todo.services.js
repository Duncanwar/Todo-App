import models from '../database/models/index';

const { Todos, Users } = models;
/**
 * @description This service deals with the User model
 */
export default class UserServices {
  /**
   * @description this service create a new todo in the db
   * @param {object} todo
   * @return {object} return the created todo
   */
  static async create(todo) {
    const todos = await Todos.create(todo);
    return todos;
  }

  static async findOneById(id) {
    const todo = await Todos.findOne({ where: { id } });
    return todo;
  }

  static async findAll() {
    return await Todos.findAll({ order: [['Todos', 'createdAt', 'ASC']] });
  }

  static async update(data, id) {
    return await Todos.update(data, { where: { id } });
  }

  static async delete(id) {
    return await Todos.destroy({ where: { id } });
  }

  static async findAllByUserId(id) {
    return await Todos.findAll({
      where: { userId: id },
      order: [
        ['id', 'DESC']
      ],
      include: [{ model: Users }],
    });
  }
}
