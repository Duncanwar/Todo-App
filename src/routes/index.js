import { Router } from 'express';
import dotenv from 'dotenv';
import user from './api/user.route';
import todo from './api/todo.route';

dotenv.config();

const router = Router();

router.get('/api/', (req, res) => { res.json({ mes: 'api' }); });
router.use('/', user);
router.use('/', todo);

export default router;
