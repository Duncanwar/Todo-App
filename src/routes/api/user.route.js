import { Router } from 'express';
import UserControllers from '../../controllers/User.controller';
import auth from '../../middlewares/auth';

const router = Router();
const { signup, login } = UserControllers;
const { verifyUserOnLogin, checkEmailExistOnSignup } = auth;

router.post('/signup', checkEmailExistOnSignup, signup);
router.post('/login', verifyUserOnLogin, login);

export default router;
