import { Router } from 'express';
import TodoController from '../../controllers/Todo.controller';
import tokenAuthentication from '../../middlewares/tokenAuthentication';

const router = Router();

router.post('/todos', [tokenAuthentication], TodoController.addTodo);
router.get('/todos', [tokenAuthentication], TodoController.getAll);
router.get('/todos/user', [tokenAuthentication], TodoController.getTodoListByUser);
router.get('/todos/:id', [tokenAuthentication], TodoController.getOneById);
router.put('/todos/:id', [tokenAuthentication], TodoController.update);
router.delete('/todos/:id', [tokenAuthentication], TodoController.deleteOne);

export default router;
