import UserServices from '../services/User.services';
import helpers from '../utils/helpers';
import responses from '../utils/responses';
import statusCode from '../utils/statusCode';
import customMessage from '../utils/customMessage';

const { getUserByIdOrEmail } = UserServices;
const { errorResponse } = responses;
const { conflict } = statusCode;
const { duplicateEmail } = customMessage;
const { comparePassword } = helpers;

const checkEmailExistOnSignup = async (req, res, next) => {
  const { name, email, password } = req.body;
  if (!password || !name || !email) {
    return errorResponse(res, 401, 'please fill the field');
  }
  const user = await getUserByIdOrEmail(req.body.email);
  if (user) {
    return errorResponse(res, conflict, duplicateEmail);
  }
  return next();
};

const verifyUserOnLogin = async (req, res, next) => {
  const user = await getUserByIdOrEmail(req.body.email);
  if (!user) {
    return errorResponse(res, 401, 'Invalid email or password');
  }
  const password = await comparePassword(req.body.password, user.password);
  if (!password) {
    return errorResponse(res, 401, 'Invalid password or email');
  }
  return next();
};
export default { checkEmailExistOnSignup, verifyUserOnLogin };
