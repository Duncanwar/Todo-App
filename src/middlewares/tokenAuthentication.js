import helper from '../utils/helpers';
import responses from '../utils/responses';

const { verifyToken } = helper;

export default async (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return responses.errorResponse(res, 401, 'You must be logged in');
  }
  const token = authorization.replace('Bearer ', '');
  const payload = verifyToken(token);
  req.User = payload;
  return next();
};
